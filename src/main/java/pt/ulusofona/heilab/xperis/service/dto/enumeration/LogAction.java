package pt.ulusofona.heilab.xperis.service.dto.enumeration;

public enum LogAction {
    UPDATE,
    UPDATE_DATA,
    DOWNLOAD_DATA,
}
