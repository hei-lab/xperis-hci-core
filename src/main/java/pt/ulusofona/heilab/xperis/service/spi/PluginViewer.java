package pt.ulusofona.heilab.xperis.service.spi;

import pt.ulusofona.heilab.xperis.service.dto.ExperimentMetaDataDTO;

public interface PluginViewer {
    byte[] view(ExperimentMetaDataDTO experimentMetaData);
    String getMimeTypeIn();
    String getMimeTypeOut();

}
