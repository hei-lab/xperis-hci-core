package pt.ulusofona.heilab.xperis.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pt.ulusofona.heilab.xperis.service.dto.enumeration.ExperimentStatus;

import java.time.Instant;
import java.util.Set;

@Getter
@Setter
@Builder
public class ExperimentDTO {

    private String id;
    private String name;
    private String description;
    private Instant startDate;
    private Instant finishCaptureDate;
    private Instant finishDate;
    private ExperimentStatus status;
    private Set<ExperimentMetaDataDTO> experimentMetaData;

}
