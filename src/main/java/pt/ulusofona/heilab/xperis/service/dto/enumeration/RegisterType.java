package pt.ulusofona.heilab.xperis.service.dto.enumeration;

/**
 * The RegisterType enumeration.
 */
public enum RegisterType {
    NO_REGISTER,
    TOKEN,
    SELF_REGISTER,
}
