package pt.ulusofona.heilab.xperis.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
public class ExperimentMetaDataDTO {

    private String id;
    private String description;
    private String dataStructure;
    private String viewerClasses;
    private Set<ExperimentDataDTO> experimentData;
    private DataMimeTypeDTO dataMimeType;
}
