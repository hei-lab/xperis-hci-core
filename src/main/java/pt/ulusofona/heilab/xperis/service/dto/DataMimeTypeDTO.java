package pt.ulusofona.heilab.xperis.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DataMimeTypeDTO {

    private String id;
    private String type;
    private String description;
    private String fileExtension;
}
