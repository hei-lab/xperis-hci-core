package pt.ulusofona.heilab.xperis.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Builder
public class ExperimentDataDTO {

    private String id;
    private String participants;
    private byte[] data;
    private String dataContentType;
    private Instant createdDate;
}
